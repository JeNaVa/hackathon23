package com.example.hackatonapp.Materials

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.Fragments.Material.CreateMaterial
import com.example.hackatonapp.Fragments.Material.DataMaterial
import com.example.hackatonapp.Fragments.Material.MaterialItemAdapter
import com.example.hackatonapp.main.HomeActivity
import com.example.hackatonapp.R
import org.json.JSONArray
import kotlin.concurrent.thread

class MaterialsActivity : AppCompatActivity() {
    private val materialsList = arrayListOf<DataMaterial>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_materials)
        val backButton: View = findViewById(R.id.backButton)
        val newMaterialButton: Button = findViewById(R.id.newMaterialButton)
        val list: RecyclerView = findViewById(R.id.recView)

        list.layoutManager = LinearLayoutManager(this)
        //list.adapter = MaterialItemAdapter(materialsList, this, fragmentManager = fragmentManager)

        getMaterial(list)
        newMaterialButton.setOnClickListener {
            val intent = Intent(this, CreateMaterial::class.java)
            startActivity(intent)
        }


        backButton.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }

    }

    fun addMaterial(name: String, count: Int, uriImage: Uri) {
        materialsList.add(DataMaterial(name, "",count, "", uriImage))
    }
    fun addMaterial(name: String, count: Int) {
        materialsList.add(DataMaterial(name, "",count))
    }


    private fun getMaterial(list: RecyclerView)
    {
        thread {
            lateinit var materials: JSONArray
            var name = arrayListOf<String>()
            var count = arrayListOf<Int>()
            var uri = arrayListOf<String>()
            val url = "http://10.0.2.2:8000/api/material/index"
            val queue = Volley.newRequestQueue(this)
            val requestPost = object : StringRequest(
                Method.GET, url,
                {
                        response ->
                    materials = JSONArray(response)
                    for (i in 0..materials.length() - 1) {
                        name.add(materials.getJSONObject(i).getString("name"))
                        count.add(materials.getJSONObject(i).getString("count").toInt())
                        uri.add(materials.getJSONObject(i).getString("img"))
                        Log.d("MyUriMaterial", "$uri")
                        addMaterial(name[i], count[i], Uri.parse(uri[i]))
                        list.adapter?.notifyDataSetChanged()
                    }
                },
                {
                        response -> Log.d("MyResponseErr", "$response")
                }
            ) {

            }
            queue.add(requestPost)
        }
    }







}