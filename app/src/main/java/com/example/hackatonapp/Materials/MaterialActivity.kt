package com.example.hackatonapp.Materials

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.R
import org.json.JSONArray
import kotlin.concurrent.thread

class MaterialActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_material)
        val materialImage: ImageView = findViewById(R.id.materialImage)
        val materialDesc: TextView = findViewById(R.id.materialDesc)
        val materialCount: Button = findViewById(R.id.materialCount)
        val backButton: ImageView = findViewById(R.id.backButton)
        val image = intent.getStringExtra("materialUriImage")

        if (image != null) {
            val parsedImage = Uri.parse(intent.getStringExtra("materialUriImage"))
            materialImage.setImageURI(parsedImage)
        }else {
            materialImage.setImageResource(intent.getIntExtra("image", 0))
        }
        materialDesc.text = intent.getStringExtra("materialDesc")
        materialCount.text= intent.getStringExtra("materialCount")

        backButton.setOnClickListener{
            val intent = Intent(this, MaterialsActivity::class.java)
            startActivity(intent)
        }
    }
}