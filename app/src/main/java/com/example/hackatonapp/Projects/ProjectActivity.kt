package com.example.hackatonapp.Projects

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.R
import org.json.JSONArray
import kotlin.concurrent.thread

class ProjectActivity : AppCompatActivity() {

    val worksItem = arrayListOf<DataWork>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project)
        val progress: Button = findViewById(R.id.progress)
        val graphButton: Button = findViewById(R.id.graphButton)
        val projectDesc: TextView = findViewById(R.id.projectDesc)
        val projectImage: ImageView = findViewById(R.id.projectImage)
        val backButton: View = findViewById(R.id.backButton)
        val newWorkButton: Button = findViewById(R.id.addWorkButton)
        val workList: RecyclerView = findViewById(R.id.workList)
        val parsedImage = Uri.parse(intent.getStringExtra("projectImageUri"))


        if (parsedImage != null) {
            projectImage.setImageURI(parsedImage)
        } else {
            val image = intent.getIntExtra("projectImage", 0)
            projectImage.setImageResource(image)
        }

        progress.text = intent.getStringExtra("projectProgress")
        projectDesc.text = intent.getStringExtra("projectDesc")

        workList.layoutManager = LinearLayoutManager(this)
        workList.adapter = ProjectWorkAdapter(worksItem)

        getWork(workList)

        newWorkButton.setOnClickListener {
            val intent = Intent(this, AddWorkActivity::class.java)
            startActivity(intent)
        }

        backButton.setOnClickListener {
            val intent = Intent(this, ProjectsActivity::class.java)
            startActivity(intent)
        }

        graphButton.setOnClickListener {
            val intent = Intent(this, ProjectGraph::class.java)
            startActivity(intent)
        }



    }

    fun addWorkItem(name: String) {
        worksItem.add(DataWork(name))
        Log.d("MyWork", "$name")
    }

    fun getWork(workList: RecyclerView) {
        thread {
            lateinit var tasks: JSONArray
            var taskName = arrayListOf<String>()
            val url = "http://10.0.2.2:8000/api/task/index"
            val queue = Volley.newRequestQueue(this)
            val requestPost = object : StringRequest(
                Method.GET, url,
                {
                        response ->
                    tasks = JSONArray(response)
                    Log.d("MyWorkJSON", "$tasks")
                    for (i in 0..tasks.length()-1) {
                        taskName.add(tasks.getJSONObject(i).getString("work_name"))
                        addWorkItem(taskName[i])
                        workList.adapter?.notifyDataSetChanged()
                    }
                    Log.d("MyResponseAc", "$response")
                },
                {
                        response -> Log.d("MyResponseErr", "$response")
                }
            ) {

            }
            queue.add(requestPost)
        }
    }
}
