package com.example.hackatonapp.Projects

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.Fragments.Project.ProjectItemAdapter
import com.example.hackatonapp.main.HomeActivity
import com.example.hackatonapp.R
import org.json.JSONArray
import kotlin.concurrent.thread

class ProjectsActivity : AppCompatActivity() {
    val items = arrayListOf<DataProject>()
    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_projects)
        val list: RecyclerView = findViewById(R.id.recView)
        val backButton: View = findViewById(R.id.backButton)
        val newProjectButton: Button = findViewById(R.id.newProjectButton)

//        list.layoutManager = LinearLayoutManager(this)
//        list.adapter = ProjectItemAdapter(items, this, )


        getProject(list)

        backButton.setOnClickListener{
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }

        newProjectButton.setOnClickListener {
            val intent = Intent(this, CreateProjectActivity::class.java)
            startActivity(intent)
        }

    }


    fun addItem( name: String, desc: String, progress: Int, image: String, imageUri: Uri) {
        items.add(DataProject(name, desc, progress, image, imageUri))
    }

    fun addItem( name: String, desc: String, imageUri: Uri) {
        items.add(DataProject(name, desc, 0, "place_photo1", imageUri))
    }

    fun addItem( name: String, desc: String, progress: Int, image: String) {
        items.add(DataProject(name, desc, progress, image))
    }

    fun addItem(name: String, desc: String, image: String) {
        items.add(DataProject(name, desc, 0, image))
    }

    fun addItem(name: String, desc: String, ) {
        items.add(DataProject(name, desc, 0))
    }

    fun getProject(list: RecyclerView)
    {
        thread {
            lateinit var projects: JSONArray
            var name = arrayListOf<String>()
            var descr = arrayListOf<String>()
            var uri = arrayListOf<String>()
            val url = "http://10.0.2.2:8000/api/project/index"
            val queue = Volley.newRequestQueue(this)
            val requestPost = object : StringRequest(
                Method.GET, url,
                {
                        response ->
                    projects = JSONArray(response)
                    for (i in 0..projects.length() - 1) {
                        name.add(projects.getJSONObject(i).getString("name"))
                        descr.add(projects.getJSONObject(i).getString("descr"))
                        uri.add(projects.getJSONObject(i).getString("img"))
                        addItem(name[i], descr[i], Uri.parse(uri[i]))
                        list.adapter?.notifyDataSetChanged()
                    }
                },
                {
                        response -> Log.d("MyResponseErr", "$response")
                }
            ) {

            }
            queue.add(requestPost)
            for (i in name.indices) {
                addItem(name[i], descr[i])
            }
        }
    }


}

