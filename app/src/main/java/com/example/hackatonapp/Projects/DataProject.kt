package com.example.hackatonapp.Projects

import android.net.Uri
import java.io.Serializable

class DataProject (
    val name: String, // House 1
    val description: String, // This is a big project
    val workProgress: Int = 0, // 20%, 40%
    val image: String = "place_photo1",
    val uriImage: Uri? = null,
) : Serializable {

}