package com.example.hackatonapp.Projects

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.Fragments.Project.ProjectsFragment
import com.example.hackatonapp.R
import com.example.hackatonapp.main.HomeActivity
import com.github.dhaval2404.imagepicker.ImagePicker
import org.json.JSONException
import org.json.JSONObject

class CreateProjectActivity : AppCompatActivity() {
    var uri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_project)
        val projectName: TextView = findViewById(R.id.projectNameInput)
        val projectDesc: TextView = findViewById(R.id.projectDescInput)
        val createButton: Button = findViewById(R.id.buttonCreateProject)
        val backButton: ImageView = findViewById(R.id.back)
        val projectImageInput: ImageView = findViewById(R.id.projectImageInput)


        createButton.setOnClickListener {
            val sProjectName = projectName.text.toString()
            val sProjectDesc = projectDesc.text.toString()
            val intent = Intent(this, HomeActivity::class.java)

            setProjectInfo(sProjectName, sProjectDesc)
            startActivity(intent)
        }

        backButton.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)

            startActivity(intent)
        }


        supportActionBar?.setBackgroundDrawable(ColorDrawable(getColor(R.color.tinkoff)))
        projectImageInput.setOnClickListener {
            ImagePicker.with(this)
                .crop()	    			        //Crop image(Optional), Check Customization for more option
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val projectImageInput: ImageView = findViewById(R.id.projectImageInput)
        uri = data?.data!!
        projectImageInput.setImageURI(uri)
    }

    fun setProjectInfo(projectName: String, projectDesc: String) {
        try {
            val url = "http://10.0.2.2:8000/api/project/create"
            var jsonBody = JSONObject()
            jsonBody.put("name", projectName)
            jsonBody.put("descr", projectDesc)
            jsonBody.put("img", uri.toString())
            val requestBody = jsonBody.toString()
            val queue = Volley.newRequestQueue(this)
            val requestPost = object : StringRequest(
                Method.POST, url,
                { response ->

                },
                { response ->
                    Log.d("MyResponseErr123", "$response")
                }
            ) {
                override fun getBodyContentType(): String {
                    return "application/json; charset=utf-8"
                }

                override fun getBody(): ByteArray {
                    return requestBody.toByteArray(Charsets.UTF_8)
                }
            }
            queue.add(requestPost)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
}