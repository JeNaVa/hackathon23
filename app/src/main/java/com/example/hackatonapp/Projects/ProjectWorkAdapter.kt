package com.example.hackatonapp.Projects

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.hackatonapp.R

class ProjectWorkAdapter (
    var items: List<DataWork>,
) : RecyclerView.Adapter<ProjectWorkAdapter.MyViewHolder>() {

    class MyViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val workDesc: TextView = view.findViewById(R.id.workDesc)
        val minusWorkButton: Button = view.findViewById(R.id.minusWorkButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        Log.d("Adapter", "onCreateViewHolder")
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_project_work, parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Log.d("Adapter", "onBindViewHolder")
        holder.workDesc.text = items[position].workDesc
    }
}