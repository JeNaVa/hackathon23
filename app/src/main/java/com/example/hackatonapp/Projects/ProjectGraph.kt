package com.example.hackatonapp.Projects

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.compose.ui.graphics.Color
import com.example.hackatonapp.R
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.RadarData
import com.github.mikephil.charting.data.RadarDataSet
import com.github.mikephil.charting.data.RadarEntry
import com.github.mikephil.charting.utils.ColorTemplate

class ProjectGraph : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project_graph)
        val barChart = findViewById<BarChart>(R.id.barChart)

        val barEntry = arrayListOf<BarEntry>()
        val barDataSet = BarDataSet(barEntry, "Отчётность")
        val barData = BarData(barDataSet)
        barChart.setData(barData)
        barDataSet.setColors(ColorTemplate.COLORFUL_COLORS.toMutableList())
        barDataSet.setValueTextColor(ColorTemplate.rgb("#000000"))
        barDataSet.setValueTextSize(20f)
        barEntry.add(BarEntry(2f,3f))
        barEntry.add(BarEntry(3f,4f))

    }

}