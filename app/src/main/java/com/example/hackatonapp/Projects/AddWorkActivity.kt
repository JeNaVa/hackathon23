package com.example.hackatonapp.Projects

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.R
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import kotlin.concurrent.thread

class AddWorkActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_work)
        val backButton: ImageView = findViewById(R.id.back)
        val createButton: Button = findViewById(R.id.buttonCreateWork)
        val workName: TextView = findViewById(R.id.workNameInput)

        backButton.setOnClickListener {
            val intent = Intent(this, ProjectsActivity::class.java)
            startActivity(intent)
        }

        createButton.setOnClickListener {
            val intent = Intent(this, ProjectsActivity::class.java)
            setTask(workName.text.toString())
            startActivity(intent)
        }

    }

    fun setTask(workName: String) {
        try {
            val url = "http://10.0.2.2:8000/api/task/create"
            var jsonBody = JSONObject()
            jsonBody.put("project_id", 1)
            jsonBody.put("type_id", 1)
            jsonBody.put("status_id", 1)
            jsonBody.put("work_name", workName)
            jsonBody.put("prod_name", workName)
            jsonBody.put("units", "-")
            jsonBody.put("count", "1")
            jsonBody.put("supplier_or_performer", "Анлим")
            val requestBody = jsonBody.toString()
            val queue = Volley.newRequestQueue(this)
            val requestPost = object : StringRequest(
                Method.POST, url,
                { response ->
                    Log.d("MyResponseAcWork", "$response")
                },
                { response ->
                    Log.d("MyResponseErrWork", "$response")
                }
            ) {
                override fun getBodyContentType(): String {
                    return "application/json; charset=utf-8"
                }

                override fun getBody(): ByteArray {
                    return requestBody.toByteArray(Charsets.UTF_8)
                }
            }
            queue.add(requestPost)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }



}