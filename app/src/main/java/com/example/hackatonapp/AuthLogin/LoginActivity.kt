package com.example.hackatonapp.AuthLogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.R
import com.example.hackatonapp.main.HomeActivity
import com.example.hackatonapp.main.MainActivity
import com.google.gson.JsonObject
import org.json.JSONException
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val email: TextView = findViewById(R.id.emailInput)
        val password: TextView = findViewById(R.id.passwordInput)
        val forgotPasswordButton: TextView = findViewById(R.id.buttonForgotPassword)
        val signUpButton: TextView = findViewById(R.id.onRegistrationActivity)
        val backButton: View = findViewById(R.id.back)
        val LogInButton: Button = findViewById(R.id.logInButton)

        signUpButton.setOnClickListener {
            val intent = Intent(this, AuthActivity::class.java)
            startActivity(intent)
        }

        LogInButton.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            var url = "http://10.0.2.2:8000/api/user/login"
            var status: Boolean = false
            try {
                var jsonBody = JSONObject()
                jsonBody.put("email", email.text.toString())
                jsonBody.put("password", password.text.toString())
                val requestBody = jsonBody.toString()
                val queue = Volley.newRequestQueue(this)
                val requestPost = object : StringRequest(
                    Method.POST, url,
                    { response ->
                        val obj = JSONObject(response)
                        status = obj.getString("status").toBoolean()
                        Log.d("MyResponseStatus", "$status")
                        if (status) {
                            startActivity(intent)
                        } else{
                            Toast.makeText(this, "Неверный логин или пароль", Toast.LENGTH_SHORT).show()
                        }
                        //Log.d("MyResponseAc", "$response")
                        //Log.d("MyResponseAcStatus", "$status")
                    },
                    { response ->
                        //Log.d("MyResponseErr", "$response")
                    }
                ) {
                    override fun getBodyContentType(): String {
                        return "application/json; charset=utf-8"
                    }

                    override fun getBody(): ByteArray {
                        return requestBody.toByteArray(Charsets.UTF_8)
                    }
                }
                //Log.d("MyResponse", "queue")
                queue.add(requestPost)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }


        backButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}