package com.example.hackatonapp.AuthLogin

import android.app.DownloadManager.Request
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.R
import com.example.hackatonapp.main.MainActivity
import org.json.JSONException
import org.json.JSONObject

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        val name: TextView = findViewById(R.id.nameInput)
        val email: TextView = findViewById(R.id.emailInput)
        val password: TextView = findViewById(R.id.passwordInput)
        val signUpButton: Button = findViewById(R.id.buttonSignUp)
        val backButton: View = findViewById(R.id.back)
        val logInnedButton: TextView = findViewById(R.id.onLogInActivity)

        backButton.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        logInnedButton.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        signUpButton.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            try {
                var jsonBody = JSONObject()
                jsonBody?.put("name", name.text.toString())
                jsonBody?.put("email", email.text.toString())
                jsonBody?.put("password", password.text.toString())
                jsonBody?.put("password_confirm", password.text.toString())
                val requestBody = jsonBody.toString()
                val url = "http://10.0.2.2:8000/api/user/register"
                val queue = Volley.newRequestQueue(this)
                val requestPost = object : StringRequest(
                    Method.POST, url,
                    {
                            response -> Log.d("MyResponseAc", "$response")
                    },
                    {
                            response -> Log.d("MyResponseErr", "$response")
                    }
                ) {
                    override fun getBodyContentType(): String {
                        return "application/json; charset=utf-8"
                    }

                    override fun getBody(): ByteArray {
                            return requestBody.toByteArray(Charsets.UTF_8)
                    }
                }
                //Log.d("MyResponse", "queue")
                queue.add(requestPost)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            startActivity(intent)
        }


    }
}