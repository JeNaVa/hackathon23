package com.example.hackatonapp.Fragments.Project

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hackatonapp.Projects.DataProject
import com.example.hackatonapp.R

class ProjectItemAdapter(
    var items: List<DataProject>,
    var context: Context?,
    val fragmentManager: FragmentManager,
) : RecyclerView.Adapter<ProjectItemAdapter.MyViewHolder>() {

    class MyViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val projectName: TextView = view.findViewById(R.id.projectDesc)
        val projectProgress: TextView = view.findViewById(R.id.projectProgress)
        val project: View = view.findViewById(R.id.project)
        val image: ImageView = view.findViewById(R.id.projectImage)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        Log.d("Adapter", "onCreateViewHolder")
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_project_item, parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Log.d("Adapter", "onBindViewHolder")
        holder.projectName.text = items[position].name
        holder.projectProgress.text = "${items[position].workProgress}%"
        Log.d("imageUri", "${items[position].uriImage}")
        val imageId = context?.resources!!.getIdentifier(
            items[position].image,
            "drawable",
            context?.packageName
        )
        if (items[position].uriImage != null) {
            holder.image.setImageURI(items[position].uriImage)
        }else {
            holder.image.setImageResource(imageId)
        }

        holder.project.setOnClickListener {

            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, ProjectFragment(
                items[position].description,
                items[position].workProgress,
                imageId,
                items[position].uriImage,
            ))

            transaction?.commit()
        }


    }

}