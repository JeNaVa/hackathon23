package com.example.hackatonapp.Fragments.Material

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.Fragments.HomeFragment
import com.example.hackatonapp.R
import org.json.JSONArray
import kotlin.concurrent.thread


class MaterialsFragment : Fragment() {
    private val materialsList = arrayListOf<DataMaterial>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_materials, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val backButton: ImageView = view.findViewById(R.id.backButton)
        val newMaterialButton: Button = view.findViewById(R.id.newMaterialButton)

        val rcView: RecyclerView = view.findViewById(R.id.recView)

        rcView.layoutManager = LinearLayoutManager(context)
        rcView.adapter = context?.let { fragmentManager?.let { it1 -> MaterialItemAdapter(materialsList, it, fragmentManager = it1) } }

        getMaterial(rcView)

        backButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, HomeFragment.newInstance())
            transaction?.commit()
        }

        newMaterialButton.setOnClickListener {
            val intent = Intent(context, CreateMaterial::class.java)
            startActivity(intent)
        }

    }

    fun addMaterial(name: String, count: Int, uriImage: Uri) {
        materialsList.add(DataMaterial(name, "",count, "", uriImage))
    }
    fun addMaterial(name: String, count: Int) {
        materialsList.add(DataMaterial(name, "",count))
    }

    private fun getMaterial(list: RecyclerView)
    {
        thread {
            lateinit var materials: JSONArray
            var name = arrayListOf<String>()
            var count = arrayListOf<Int>()
            var uri = arrayListOf<String>()
            val url = "http://10.0.2.2:8000/api/material/index"
            val queue = Volley.newRequestQueue(context)
            val requestPost = object : StringRequest(
                Method.GET, url,
                {
                        response ->
                    materials = JSONArray(response)
                    Log.d("MyMaterials", "$materials")
                    for (i in 0..materials.length() - 1) {
                        name.add(materials.getJSONObject(i).getString("name"))
                        count.add(materials.getJSONObject(i).getString("count").toInt())
                        uri.add(materials.getJSONObject(i).getString("img"))
                        Log.d("MyUriMaterial", "$uri")
                        addMaterial(name[i], count[i], Uri.parse(uri[i]))
                        list.adapter?.notifyDataSetChanged()
                    }
                },
                {
                        response -> Log.d("MyResponseErr", "$response")
                }
            ) {

            }
            queue.add(requestPost)
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = MaterialsFragment()
    }
}