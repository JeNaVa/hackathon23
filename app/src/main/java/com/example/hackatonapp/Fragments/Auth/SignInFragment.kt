package com.example.hackatonapp.Fragments.Auth

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.Fragments.MainFragment
import com.example.hackatonapp.R
import org.json.JSONException
import org.json.JSONObject


class SignInFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val userName: TextView = view.findViewById(R.id.nameInput)
        val email: TextView = view.findViewById(R.id.emailInput)
        val userPassword: TextView = view.findViewById(R.id.passwordInput)

        val signUpButton: Button = view.findViewById(R.id.buttonSignUp)
        val backButton: ImageView = view.findViewById(R.id.backButton)
        val logInButton: TextView = view.findViewById(R.id.onLogInFragment)

        signUpButton.setOnClickListener {
            val name = userName.text.toString()
            val userEmail = email.text.toString()
            val password = userPassword.text.toString()

            signUp(name, userEmail, password)
        }

        backButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, MainFragment.newInstance())
            transaction?.commit()
        }

        logInButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, LogInFragment.newInstance())
            transaction?.commit()
        }

    }

    fun signUp(name: String, email: String, password: String) {
        val transaction = fragmentManager?.beginTransaction()
        try {
            var jsonBody = JSONObject()
            jsonBody?.put("name", name)
            jsonBody?.put("email", email)
            jsonBody?.put("password", password)
            jsonBody?.put("password_confirm", password)
            val requestBody = jsonBody.toString()
            val url = "http://10.0.2.2:8000/api/user/register"
            val queue = Volley.newRequestQueue(context)
            val requestPost = object : StringRequest(
                Method.POST, url,
                {
                        response -> Log.d("MyResponseAc", "$response")
                },
                {
                        response -> Log.d("MyResponseErr", "$response")
                }
            ) {
                override fun getBodyContentType(): String {
                    return "application/json; charset=utf-8"
                }

                override fun getBody(): ByteArray {
                    return requestBody.toByteArray(Charsets.UTF_8)
                }
            }
            queue.add(requestPost)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        transaction?.replace(R.id.mainContainer, LogInFragment.newInstance())
        transaction?.commit()

    }


    companion object {

        @JvmStatic
        fun newInstance() = SignInFragment()
    }
}