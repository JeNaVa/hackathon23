package com.example.hackatonapp.Fragments.Project

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.example.hackatonapp.R


class CreateProjectFragment : Fragment() {
    var uri: Uri? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_project, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val projectName: TextView = view.findViewById(R.id.projectNameInput)
        val projectDescInput: TextView = view.findViewById(R.id.projectDescInput)

        val backButton: ImageView = view.findViewById(R.id.backButton)
        val createProjectButton: Button = view.findViewById(R.id.buttonCreateProject)

        val projectImageInput: ImageView = view.findViewById(R.id.projectImageInput)

        backButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, ProjectFragment.newInstance())
            transaction?.commit()
        }


    }

    companion object {

        @JvmStatic
        fun newInstance() = CreateProjectFragment()
    }
}