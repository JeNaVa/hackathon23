package com.example.hackatonapp.Fragments.Project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.hackatonapp.R


class ProjectItemFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_project_item, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance() = ProjectItemFragment()
    }
}