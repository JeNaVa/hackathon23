package com.example.hackatonapp.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.hackatonapp.Fragments.Material.MaterialsFragment
import com.example.hackatonapp.Fragments.Project.ProjectsFragment
import com.example.hackatonapp.R


class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val projectImageButton: Button = view.findViewById(R.id.ProjectImageButton)
        val materialImageButton: Button = view.findViewById(R.id.MaterialImageButton)

        projectImageButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, ProjectsFragment.newInstance())
            transaction?.commit()
        }

        materialImageButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, MaterialsFragment.newInstance())
            transaction?.commit()
        }



    }


    companion object {

        @JvmStatic
        fun newInstance() = HomeFragment()
    }
}