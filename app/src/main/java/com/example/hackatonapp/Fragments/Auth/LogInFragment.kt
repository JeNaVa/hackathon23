package com.example.hackatonapp.Fragments.Auth

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.Fragments.HomeFragment
import com.example.hackatonapp.Fragments.MainFragment
import com.example.hackatonapp.R
import org.json.JSONException
import org.json.JSONObject


class LogInFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_log_in, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val userEmail: TextView = view.findViewById(R.id.emailInput)
        val userPassword: TextView = view.findViewById(R.id.passwordInput)
        val onRegistrationButton: TextView = view.findViewById(R.id.onRegistrationFragment)
        val backButton: ImageView = view.findViewById(R.id.back)
        val logInButton: Button = view.findViewById(R.id.logInButton)

        logInButton.setOnClickListener {
            val email = userEmail.text.toString()
            val password = userPassword.text.toString()
            logIn(email, password)
        }

        backButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, MainFragment.newInstance())
            transaction?.commit()
        }

        onRegistrationButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, SignInFragment.newInstance())
            transaction?.commit()
        }
    }

    fun logIn(email: String, password: String) {

        var url = "http://10.0.2.2:8000/api/user/login"
        var status: Boolean = false
        val transaction = fragmentManager?.beginTransaction()
        try {
            var jsonBody = JSONObject()
            jsonBody.put("email", email)
            jsonBody.put("password", password)
            val requestBody = jsonBody.toString()
            val queue = Volley.newRequestQueue(context)
            val requestPost = object : StringRequest(
                Method.POST, url,
                { response ->
                    val obj = JSONObject(response)
                    status = obj.getString("status").toBoolean()
                    Log.d("MyResponseStatus", "$status")
                    if (status) {
                        transaction?.replace(R.id.mainContainer, HomeFragment.newInstance())
                        transaction?.commit()
                    } else{
                        Toast.makeText(context, "Неверный логин или пароль", Toast.LENGTH_SHORT).show()
                    }
                    //Log.d("MyResponseAc", "$response")
                    //Log.d("MyResponseAcStatus", "$status")
                },
                { response ->
                    //Log.d("MyResponseErr", "$response")
                }
            ) {
                override fun getBodyContentType(): String {
                    return "application/json; charset=utf-8"
                }

                override fun getBody(): ByteArray {
                    return requestBody.toByteArray(Charsets.UTF_8)
                }
            }
            //Log.d("MyResponse", "queue")
            queue.add(requestPost)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }






    companion object {

        @JvmStatic
        fun newInstance() = LogInFragment()
    }
}