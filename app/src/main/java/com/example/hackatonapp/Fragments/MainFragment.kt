package com.example.hackatonapp.Fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.example.hackatonapp.Fragments.Auth.LogInFragment
import com.example.hackatonapp.Fragments.Auth.SignInFragment
import com.example.hackatonapp.R
import com.example.hackatonapp.main.HomeActivity

class MainFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val logInButton: Button = view.findViewById(R.id.buttonLogIn)
        val signInButton: Button = view.findViewById(R.id.buttonSignIn)
        val logo: ImageView = view.findViewById(R.id.logo)


        logInButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, LogInFragment.newInstance())
            transaction?.commit()
        }

        signInButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, SignInFragment.newInstance())
            transaction?.commit()
        }

        logo.setOnClickListener {
            val intent = Intent(context, HomeActivity::class.java)
            startActivity(intent)
        }


    }


    companion object {

        @JvmStatic
        fun newInstance() = MainFragment()
    }
}