package com.example.hackatonapp.Fragments.Project

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.Fragments.HomeFragment
import com.example.hackatonapp.Projects.CreateProjectActivity
import com.example.hackatonapp.Projects.DataProject
import com.example.hackatonapp.R
import org.json.JSONArray
import kotlin.concurrent.thread


class ProjectsFragment : Fragment() {
    val items = arrayListOf<DataProject>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_projects, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val backButton: ImageView = view.findViewById(R.id.backButton)
        val newProjectButton: Button = view.findViewById(R.id.newProjectButton)

        val rcView: RecyclerView = view.findViewById(R.id.rcView)

        rcView.layoutManager = LinearLayoutManager(context)
        rcView.adapter = ProjectItemAdapter(items, context, fragmentManager = requireFragmentManager())

        getProjects(rcView)

        backButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, HomeFragment.newInstance())
            transaction?.commit()
        }

        newProjectButton.setOnClickListener {
            val intent = Intent(this.context, CreateProjectActivity::class.java)
            startActivity(intent)
        }

    }

    fun addItem( name: String, desc: String, progress: Int, image: String, imageUri: Uri) {
        items.add(DataProject(name, desc, progress, image, imageUri))
    }

    fun addItem( name: String, desc: String, imageUri: Uri) {
        items.add(DataProject(name, desc, 0, "place_photo1", imageUri))
    }

    fun addItem( name: String, desc: String, progress: Int, image: String) {
        items.add(DataProject(name, desc, progress, image))
    }

    fun addItem(name: String, desc: String, image: String) {
        items.add(DataProject(name, desc, 0, image))
    }

    fun addItem(name: String, desc: String, ) {
        items.add(DataProject(name, desc, 0))
    }

    fun getProjects(list: RecyclerView)
    {
        thread {
            lateinit var projects: JSONArray
            var name = arrayListOf<String>()
            var descr = arrayListOf<String>()
            var uri = arrayListOf<String>()
            val url = "http://10.0.2.2:8000/api/project/index"
            val queue = Volley.newRequestQueue(context)
            val requestPost = object : StringRequest(
                Method.GET, url,
                {
                        response ->
                    projects = JSONArray(response)
                    for (i in 0..projects.length() - 1) {
                        name.add(projects.getJSONObject(i).getString("name"))
                        descr.add(projects.getJSONObject(i).getString("descr"))
                        uri.add(projects.getJSONObject(i).getString("img"))
                        addItem(name[i], descr[i], Uri.parse(uri[i]))
                        list.adapter?.notifyDataSetChanged()
                    }
                },
                {
                        response -> Log.d("MyResponseErr", "$response")
                }
            ) {

            }
            queue.add(requestPost)
            for (i in name.indices) {
                addItem(name[i], descr[i])
            }
        }
    }


    companion object {

        @JvmStatic
        fun newInstance() = ProjectsFragment()
    }
}