package com.example.hackatonapp.Fragments.Material

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.Materials.MaterialsActivity
import com.example.hackatonapp.R
import com.example.hackatonapp.main.HomeActivity
import com.github.dhaval2404.imagepicker.ImagePicker
import org.json.JSONException
import org.json.JSONObject

class CreateMaterial : AppCompatActivity() {
    var uri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_material)
        val materialName: TextView = findViewById(R.id.materialNameInput)
        val materialDesc: TextView = findViewById(R.id.materialCountInput)
        val createButton: Button = findViewById(R.id.buttonCreateProject)
        val materialImageInput: ImageView = findViewById(R.id.materialImageInput)

        createButton.setOnClickListener {
            val sMaterialName = materialName.text.toString()
            val sMaterialDesc = materialDesc.text.toString()
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            setMaterialInfo(sMaterialName, sMaterialDesc)
        }

        supportActionBar?.setBackgroundDrawable(ColorDrawable(getColor(R.color.tinkoff)))
        materialImageInput.setOnClickListener {
            ImagePicker.with(this)
                .crop()	    			        //Crop image(Optional), Check Customization for more option
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val materialImageInput: ImageView = findViewById(R.id.materialImageInput)
        uri = data?.data!!
        materialImageInput.setImageURI(uri)
    }

    fun setMaterialInfo(materialName: String, materialCount: String) {
        try {
            val url = "http://10.0.2.2:8000/api/material/create"
            var jsonBody = JSONObject()
            jsonBody.put("name", materialName)
            jsonBody.put("count", materialCount)
            jsonBody.put("img", uri.toString())
            val requestBody = jsonBody.toString()
            val queue = Volley.newRequestQueue(this)
            val requestPost = object : StringRequest(
                Method.POST, url,
                { response ->
                    Log.d("MyCreateMatResponse", "$response")
                },
                { response ->
                    Log.d("MyResponseErr123", "$response")
                }
            ) {
                override fun getBodyContentType(): String {
                    return "application/json; charset=utf-8"
                }

                override fun getBody(): ByteArray {
                    return requestBody.toByteArray(Charsets.UTF_8)
                }
            }
            queue.add(requestPost)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
}