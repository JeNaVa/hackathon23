package com.example.hackatonapp.Fragments.Project

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.hackatonapp.Projects.DataWork
import com.example.hackatonapp.Projects.ProjectWorkAdapter
import com.example.hackatonapp.R
import org.json.JSONArray
import kotlin.concurrent.thread


class ProjectFragment(
    val projectDescription: String,
    val progress: Int,
    val image: Int = 0,
    val imageUri: Uri? = null
) : Fragment() {

    val worksItem = arrayListOf<DataWork>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_project, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val backButton: ImageView = view.findViewById(R.id.backButton)
        val graphButton: Button = view.findViewById(R.id.graphButton)
        val addWorkButton: Button = view.findViewById(R.id.addWorkButton)

        val progress: Button = view.findViewById(R.id.progress)

        val projectImage: ImageView = view.findViewById(R.id.projectImage)

        val projectDesc: TextView = view.findViewById(R.id.projectDesc)

        val workRcView: RecyclerView = view.findViewById(R.id.rvWorkList)

        workRcView.layoutManager = LinearLayoutManager(context)
        workRcView.adapter = ProjectWorkAdapter(worksItem)

        if (imageUri != null) {
            projectImage.setImageURI(imageUri)
        } else {
            projectImage.setImageResource(image.toString().toInt())
        }

        projectDesc.text = projectDescription
        progress.text = this.progress.toString()

        backButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, ProjectsFragment.newInstance())
            transaction?.commit()
        }

        addWorkButton.setOnClickListener {

        }

        getWork(workRcView)

    }


    fun addWorkItem(name: String) {
        worksItem.add(DataWork(name))
        Log.d("MyWork", "$name")
    }

    fun getWork(workList: RecyclerView) {
        thread {
            lateinit var tasks: JSONArray
            var taskName = arrayListOf<String>()
            val url = "http://10.0.2.2:8000/api/task/index"
            val queue = Volley.newRequestQueue(context)
            val requestPost = object : StringRequest(
                Method.GET, url,
                {
                        response ->
                    tasks = JSONArray(response)
                    Log.d("MyWorkJSON", "$tasks")
                    for (i in 0..tasks.length()-1) {
                        taskName.add(tasks.getJSONObject(i).getString("work_name"))
                        addWorkItem(taskName[i])
                        workList.adapter?.notifyDataSetChanged()
                    }
                    Log.d("MyResponseAc", "$response")
                },
                {
                        response -> Log.d("MyResponseErr", "$response")
                }
            ) {

            }
            queue.add(requestPost)
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = ProjectFragment("", 0)
    }
}