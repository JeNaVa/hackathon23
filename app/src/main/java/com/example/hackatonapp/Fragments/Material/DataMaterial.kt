package com.example.hackatonapp.Fragments.Material

import android.net.Uri

data class DataMaterial(
    val name: String,
    val desc: String = "",
    val count: Int = 0,
    val image: String = "bricks",
    val uriImage: Uri? = null,
)
