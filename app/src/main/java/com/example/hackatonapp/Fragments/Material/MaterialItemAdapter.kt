package com.example.hackatonapp.Fragments.Material

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hackatonapp.Materials.MaterialActivity
import com.example.hackatonapp.R


class MaterialItemAdapter(
    var items: List<DataMaterial>,
    var context: Context,
    val fragmentManager: FragmentManager,
) : RecyclerView.Adapter<MaterialItemAdapter.MyViewHolder> () {

    class MyViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val materialDesc: TextView = view.findViewById(R.id.projectDesc)
        val materialCount: TextView = view.findViewById(R.id.materialCount)
        val materialImage: ImageView = view.findViewById(R.id.materialImage)
        val material: View = view.findViewById(R.id.material)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        Log.d("Adapter", "onCreateViewHolder")
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_material_item, parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Log.d("Adapter", "onBindViewHolder")
        holder.materialDesc.text = items[position].name
        holder.materialCount.text = items[position].count.toString()

        val imageId = context.resources.getIdentifier(
            items[position].image,
            "drawable",
            context.packageName
        )

        //Log.d("MyUriAdapter", "${items[position].uriImage}")

        if (items[position].uriImage != null) {
            holder.materialImage.setImageURI(items[position].uriImage)
        }else {
            holder.materialImage.setImageResource(imageId)
        }

        holder.material.setOnClickListener {
            val transaction = fragmentManager.beginTransaction()
            transaction.replace(R.id.mainContainer, MaterialFragment.newInstance(
                items[position].name,
                items[position].count,
                imageId,
                items[position].uriImage,
                ))
            transaction.commit()
        }


    }
}
