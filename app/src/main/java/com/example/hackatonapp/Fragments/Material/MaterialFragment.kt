package com.example.hackatonapp.Fragments.Material

import android.media.Image
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.example.hackatonapp.R


class MaterialFragment(
    val name: String,
    val count: Int,
    val image: Int,
    val imageUri: Uri? = null
    ) : Fragment() {
    // TODO: Rename and change types of parameters


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_material, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainImage: ImageView = view.findViewById(R.id.materialImage)

        val backButton: ImageView = view.findViewById(R.id.backButton)

        val tvCount: Button = view.findViewById(R.id.materialCount)

        if (imageUri != null) {
            mainImage.setImageURI(imageUri)
        } else {
            mainImage.setImageResource(image)
        }

        tvCount.text = count.toString()

        backButton.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.mainContainer, MaterialsFragment.newInstance())
            transaction?.commit()
        }


    }

    companion object {

        @JvmStatic
        fun newInstance(name: String, count: Int, image: Int, imageUri: Uri?) = MaterialFragment(name, count, image, imageUri)
    }
}