package com.example.hackatonapp.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.hackatonapp.Fragments.HomeFragment
import com.example.hackatonapp.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainContainer, HomeFragment.newInstance())
            .commit()



    }
}