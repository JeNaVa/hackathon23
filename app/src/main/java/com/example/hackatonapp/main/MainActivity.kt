package com.example.hackatonapp.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.hackatonapp.AuthLogin.LoginActivity
import com.example.hackatonapp.AuthLogin.AuthActivity
import com.example.hackatonapp.Fragments.MainFragment
import com.example.hackatonapp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainContainer, MainFragment.newInstance())
            .commit()

    }
}